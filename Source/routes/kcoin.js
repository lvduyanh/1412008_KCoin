var express = require('express');
var router = express.Router();

var csrf = require('csurf');

var loginCtrl = require('../controller/loginCtrl');

/* GET home page. */
router.get('/', isLogin, function(req, res, next) {
    res.render('kcoin');
});

router.get('/received', isLogin, function (req, res, next) {
    res.render('kcoin');
});

router.get('/sent', isLogin, function (req, res, next) {
    res.render('kcoin');
});

router.get('/all', isLogin, function (req, res, next) {
    res.render('kcoin');
});

router.get('/transfer', isLogin, function (req, res, next) {
    res.render('kcoin');
});

router.get('/change_pass', isLogin, function (req, res, next) {
    res.render('kcoin');
});

router.post('/change_pass', isLogin, loginCtrl.change_pass);

var loginRouter = express.Router();
loginRouter.use(csrf());
router.use('/login', loginRouter);

loginRouter.get('/', notLogin, loginCtrl.login_get);

loginRouter.post('/',  loginCtrl.login_post, function (req, res, next) {
    if (req.session.oldUrl) {
        var oldUrl = req.session.oldUrl;
        req.session.oldUrl = null;
        res.redirect(oldUrl);
    } else {
        res.redirect('/');
    }
});

loginRouter.post('/rgt',  loginCtrl.login_put, function (req, res, next) {
    // if (req.session.oldUrl) {
    //     var oldUrl = req.session.oldUrl;
    //     req.session.oldUrl = null;
    //     res.redirect(oldUrl);
    // } else {
    //     res.redirect('/');
    // }
    res.redirect('/');
});

loginRouter.get('/out', loginCtrl.logout);

module.exports = router;

function isLogin(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.originalUrl;
    res.redirect('/kcoin/login');
}

function notLogin(req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.originalUrl;
    res.redirect('/');
}