var express = require('express');
var router = express.Router();

var kcCtrl = require('../controller/kc_api');

router.post('/transfer', kcCtrl.transfer_post);

//router.get('/all', kcCtrl.all_get);

//router.get('/recent', kcCtrl.recent_get);

router.get('/transfer', kcCtrl.transfer_get);

router.get('/info', kcCtrl.info_get);

router.get('/received', kcCtrl.received_get);

router.get('/sent', kcCtrl.sent_get);

router.post('/getCode', kcCtrl.code_post);

module.exports = router;