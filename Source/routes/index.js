var express = require('express');
var router = express.Router();
var User = require('../model/user');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/kcoin');
});

router.get('/active/:code', function(req, res, next) {
    let code = req.params.code;
    User.findOne({active: code}, function (err, us) {
        if (err || us === null) {
            res.send("Active failed.");
        }
        else {
            User.update({"_id": us._id}, {"active": 'ok'}, function (err2) {
                if (err2) {
                    console.log(err2);
                    res.send("Active failed.");
                }
                else {
                    res.send("Active sucess.");
                }
            });
        }
    });
});

module.exports = router;
