var express = require('express');
var router = express.Router();
var adCtrl = require('../controller/admin');

router.get('/', isLogin, adCtrl.admin_get);

router.get('/users', isLogin, adCtrl.admin_user_get);

router.get('/trans', isLogin, adCtrl.admin_trans_get);

module.exports = router;

function isLogin(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.originalUrl;
    res.redirect('/kcoin/login');
}

function notLogin(req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    }
    req.session.oldUrl = req.originalUrl;
    res.redirect('/');
}