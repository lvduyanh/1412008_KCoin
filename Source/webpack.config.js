const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: './raCode/kcoin/App.jsx',
    output: {
        path: __dirname,
        filename: './public/js/kcoin_bundle.js'
    },
    // plugins: [
    //     new UglifyJsPlugin()
    // ],
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-0']
                },
                test: /\.jsx?$/,
                exclude: /node_modules/
            }
        ]
    }
};