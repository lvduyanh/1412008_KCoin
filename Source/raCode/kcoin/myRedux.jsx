var redux = require('redux');

const defaultState = {
    all: null,
    user: null,
    recent: null
};

var reducer = (state = defaultState, action) => {
    switch (action.type) {
        case 'get_all':
            return getAll(state, action.data);
            break;
        case 'get_recent':
            return getRecent(state, action.data);
            break;
        case 'get_money':
            return getMoney(state, action.data);
            break;
        case 'get_info':
        return getInfo(state, action.data);
        break;
        case 'get_received':
        return getReceived(state, action.data);
        break;
        case 'get_sent':
        return getSent(state, action.data);
        break;
        default:
            return state;
    }
};

var store = redux.createStore(reducer);

module.exports = store;

function getAll(state, data) {
    if (data) {
        console.log(data);
        return {...state, all: data.data};
    }
    else {
        return {...state, all: null};
    }
}

function  getRecent(state, data) {
        if (data) {
            console.log(data);
            return {
                ...state,
                user: data.user,
                recent: data.data
            };
        }
        else {
            return {
                ...state,
                user: null,
                recent: null
            };
        }
}

function getMoney(state, data) {
        if (data) {
            console.log(data);
            return {...state, user: data.user};
        }
        else {
            return {...state, user: null};
        }
}

function getInfo(state, data) {
    if (data) {
        console.log(data);
        return {...state, user: data.user};
    }
    else {
        return {...state, user: null};
    }
}

function getReceived(state, data) {
    if (data) {
        console.log(data);
        let receive = {};
        receive.address = data.address;
        receive.trans = data.trans;
        return {...state, receive: receive};
    }
    else {
        return {...state, receive: null};
    }
}

function getSent(state, data) {
    if (data) {
        console.log(data);
        let sent = {};
        sent.trans = data.trans;
        return {...state, sent: sent};
    }
    else {
        return {...state, sent: null};
    }
}