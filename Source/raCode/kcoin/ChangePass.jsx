let React = require('react');
let {connect} = require('react-redux');
let axios = require('axios');

class ChangePass extends React.Component {
	changePass () {
		console.log("change click");
		let self = this;
		let cur = this.refs.curpass.value;
		let newP = this.refs.newpass.value;
		let renew = this.refs.renewpass.value;

		if (cur.length < 4 || newP.length < 4 || renew.length < 4) {
			alert("Password must have at least 4 characters.");
			return;
		}

		if (newP !== renew) {
			alert("New password and Re-enter new password do not match.");
			return;
		}

		axios.post('/kcoin/change_pass', {
                cur: cur,
                newP: newP
            })
            .then(function (rs) {
                if (rs) {
                    console.log(rs.data);
                    var dt = rs.data;
                    if (dt.status != 200) {
                        alert(dt.err);
                    }
                    else {
                        alert(dt.data);
                        self.refs.curpass.value = '';
                        self.refs.newpass.value = '';
                        self.refs.renewpass.value = '';
                    }
                }
            })
            .catch(function (err) {
                console.log(err);
                alert("Cant change password.");
            });
	}
    render () {
        return (
            <div className="container-fluid">
                <div className="form-group">
                    <input type="password" className="form-control mb-1" ref="curpass" placeholder="Current password" required/>
                    <input type="password" className="form-control mb-1" ref="newpass" placeholder="New password" required/>
                    <input type="password" className="form-control mb-1" ref="renewpass" placeholder="Re-enter new password" required/>
                    <button className="btn btn-primary" onClick={() => this.changePass()}>Change password</button>
                </div>
            </div>
        );
    }
}

module.exports = connect(function (state, ownProps) {
    return {};
})(ChangePass);