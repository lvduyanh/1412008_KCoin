var React = require('react');
var {connect} = require('react-redux');
var axios = require('axios');

class Receive extends React.Component {
    render () {
        let usAddress = null;
        let tmp = this.props.receive;
        if (tmp && tmp.address) {
            usAddress = (<div>
                <h3>Use this address to receive money:</h3>
                <p>{tmp.address}</p>
            </div>);
        }
        else {
            usAddress = <h3>Loading address...</h3>;
        }
        let list = null;
        if (tmp && tmp.trans) {
            list = <table class='table table-striped'>
                <thead>
                    <tr>
                        <th>No</th><th>Hash</th><th>Sender</th><th>Money</th><th>Time</th><th>Status</th>
                    </tr>
                </thead>
                <tbody>
                {
                    tmp.trans.map((data, idx) => {
                        return (
                            <tr>
                                <td>{idx + 1}</td>
                                <td className="addressWidth">{data.hash}</td>
                                <td className="addressWidth">{data.sender}</td>
                                <td>{data.money}</td>
                                <td className="addressWidth">{data.time}</td>
                                <td>{data.status}</td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </table>;
        }
        else {
            list = <p>You have not received money yet!</p>;
        }
        return (
            <div className="container-fluid">
                {usAddress}
                {list}
            </div>
        );
    }
    componentDidMount () {
        console.log('received api');
        var {dispatch} = this.props;
        axios.get('/kc_api/received')
            .then(function (rs) {
                if (rs) {
                    dispatch({type: 'get_received', data: rs.data});
                }
            })
            .catch(function (err) {
                console.log(err);
                dispatch({type: 'get_received', data: null});
            });
    }
}

module.exports = connect(function (state, ownProps) {
    return {
        receive: state.receive
    };
})(Receive);