var React = require('react');
var {connect} = require('react-redux');
var axios = require('axios');

class All extends React.Component {
    render () {
        let list = null;
        if (this.props.all) {
            list = <table class='table table-striped'>
                <thead><tr><th>Send</th><th>Receive</th><th>Money</th></tr></thead>
                <tbody>
                {
                    this.props.all.map((data, idx) => {
                        return (
                            <tr>
                                <td>{data.user1}</td>
                                <td>{data.user2}</td>
                                <td>{data.coin}</td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </table>;
        }
        else {
            list = <p>Transfer's history is empty.</p>;
        }
        return (
            <div className="container-fluid">
                <h1>All Transfers</h1>
                {list}
            </div>
        );
    }
    componentDidMount () {
        console.log('all api');
        var {dispatch} = this.props;
        axios.get('/kc_api/all')
            .then(function (rs) {
                if (rs) {
                    dispatch({type: 'get_all', data: rs.data});
                }
            })
            .catch(function (err) {
                console.log(err);
                dispatch({type: 'get_all', data: null});
            });
    }
}

module.exports = connect(function (state) {
    return {
        all: state.all
    };
})(All);