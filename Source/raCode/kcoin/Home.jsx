var React = require('react');
var {connect} = require('react-redux');
var axios = require('axios');

let {Link} = require('react-router-dom');

class Home extends React.Component {
    render () {
        let welcome = null;
        if (this.props.user) {
            welcome = (<div>
                <h3>Welcome: {this.props.user.email}</h3><br/>
                <p>Real Balance: {this.props.user.coin} KC</p>
                <p>Avaiable Balance: {this.props.user.avai_coin} KC</p>
                <p>Address: {this.props.user.address}</p>
                <p>Active: {this.props.user.active}</p>
            </div>);
        }
        else {
            welcome = <h3>Loading...</h3>;
        }
        return (
            <div className="container-fluid">
                {welcome}
                <Link to='/kcoin/change_pass'>Change password</Link>
            </div>
        );
    }
    componentDidMount () {
        console.log('recent api');
        var {dispatch} = this.props;
        axios.get('/kc_api/info')
            .then(function (rs) {
                if (rs) {
                    dispatch({type: 'get_info', data: rs.data});
                }
            })
            .catch(function (err) {
                console.log(err);
                dispatch({type: 'get_info', data: null});
            });
    }
}

module.exports = connect(function (state, ownProps) {
    return {
        user: state.user
    };
})(Home);