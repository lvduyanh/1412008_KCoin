var React = require('react');
var ReactRouterDOM = require('react-router-dom');

let Route = ReactRouterDOM.Route;
let Switch = ReactRouterDOM.Switch;
let Link = ReactRouterDOM.Link;

let Home = require('./Home.jsx');
let Receive = require('./Receive.jsx');
let Sent = require('./Sent.jsx');
let Transfer = require('./Transfer.jsx');
let ChangePass = require('./ChangePass.jsx');

class Main extends React.Component {
    render () {
        return (
            <main>
                <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <a id="dashBoard" className="navbar-brand mr-5" href="/">WALLET</a>
                    <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarsExampleDefault">
                        <Link to='/kcoin' className="btn btn-link white" role="button">Home</Link>
                        <Link to='/kcoin/received' className="btn btn-link white" role="button">Received</Link>
                        <Link to='/kcoin/sent' className="btn btn-link white" role="button">Sent</Link>
                        <Link to='/kcoin/transfer' className="btn btn-link white" role="button">Transfer</Link>
                        <a href="/kcoin/login/out" className="btn btn-link white" role="button">Log out</a>
                    </div>
                </nav>
                <div className="container-fluid ml-sm-auto pt-3 mt-5">
                    <Switch>
                        <Route exact path='/kcoin' component={Home}/>
                        <Route exact path='/kcoin/received' component={Receive}/>
                        <Route exact path='/kcoin/sent' component={Sent}/>
                        <Route exact path='/kcoin/transfer' component={Transfer}/>
                        <Route exact path='/kcoin/change_pass' component={ChangePass}/>
                    </Switch>
                </div>
            </main>
        );
    }
}

module.exports = Main;