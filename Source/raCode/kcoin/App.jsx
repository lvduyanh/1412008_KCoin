var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouterDOM = require('react-router-dom');
var BrowserRouter = ReactRouterDOM.BrowserRouter;
var {Provider} = require('react-redux');

var store = require('./myRedux.jsx');
var Main = require('./Main.jsx');

ReactDOM.render((
    <Provider store={store}>
        <BrowserRouter>
            <Main />
        </BrowserRouter>
    </Provider>
), document.getElementById('root'));