var React = require('react');
var {connect} = require('react-redux');
var axios = require('axios');

class Sent extends React.Component {
    render () {
        let tmp = this.props.sent;
        let list = null;
        if (tmp && tmp.trans) {
            list = <table class='table table-striped'>
                <thead>
                    <tr>
                        <th>No</th><th>Hash</th><th>Reciever</th><th>Money</th><th>Time</th><th>Status</th>
                    </tr>
                </thead>
                <tbody>
                {
                    tmp.trans.map((data, idx) => {
                        return (
                            <tr>
                                <td>{idx + 1}</td>
                                <td className="addressWidth">{data.hash}</td>
                                <td className="addressWidth">{data.receiver}</td>
                                <td>{data.money}</td>
                                <td className="addressWidth">{data.time}</td>
                                <td>{data.status}</td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </table>;
        }
        else {
            list = <p>You have not sent money yet!</p>;
        }
        return (
            <div className="container-fluid">
                {list}
            </div>
        );
    }
    componentDidMount () {
        console.log('sent api');
        var {dispatch} = this.props;
        axios.get('/kc_api/sent')
            .then(function (rs) {
                if (rs) {
                    dispatch({type: 'get_sent', data: rs.data});
                }
            })
            .catch(function (err) {
                console.log(err);
                dispatch({type: 'get_sent', data: null});
            });
    }
}

module.exports = connect(function (state, ownProps) {
    return {
        sent: state.sent
    };
})(Sent);