var React = require('react');
var {connect} = require('react-redux');
var axios = require('axios');

class Transfer extends React.Component {
    transfer () {
        if (!this.props.user) {
            return;
        }
        var self = this;
        console.log('trans click!!!');
        var addRec = this.refs.addRec.value;
        console.log("af");
        var mn = parseInt(this.refs.money.value);
        if (isNaN(mn)) {
            alert("Please input money amount.");
            return;
        }
        console.log("bf " + mn);
        var code = this.refs.code.value;
        var time = this.refs.time.value;
        if (addRec.length < 64) {
            alert("Invalid receive address.");
            return;
        }
        if (mn > this.props.user.avai_coin) {
            alert("You dont have enought money.");
            return;
        }
        if (code.length < 64) {
            alert("Invalid confirm code.");
            return;
        }
        axios.post('/kc_api/transfer', {
            addRec: addRec,
            money: mn,
            code: code,
            time: time
        })
        .then(function (rs) {
            if (rs) {
                console.log(rs.data);
                var dt = rs.data;
                if (dt.status != 200) {
                    alert(dt.err);
                }
                else {
                    alert(dt.data);
                    self.getMoney();
                }
            }
        })
        .catch(function (err) {
            console.log(err);
            console.log("Oh my!");
        });
    }
    sendEmail () {
        console.log('send click!!!');
        let self = this;
        var addRec = this.refs.addRec.value;
        var time = this.refs.time.value;
        if (addRec.length < 64) {
            alert("Please input receive address.");
        }
        else {
            axios.post('/kc_api/getCode', {
                address: addRec,
                time: time
            })
            .then(function (rs) {
                if (rs) {
                    console.log(rs.data);
                    var dt = rs.data;
                    if (dt.status != 200) {
                        alert(dt.err);
                    }
                    else {
                        alert(dt.data);
                    }
                }
            })
            .catch(function (err) {
                console.log(err);
                alert("Cant send email.");
            });
        }
    }
    render () {
        let welcome = null;
        if (this.props.user) {
            welcome = <h3>User: {this.props.user.email} -
            Real Balance: {this.props.user.coin} KC - 
            Available Balance: {this.props.user.avai_coin} KC</h3>;
        }
        else {
            welcome = <h1>Transfer Money!</h1>;
        }
        return (
            <div className="container-fluid">
                {welcome}
                <div className="form-group">
                    <input type="text" className="form-control mb-1" ref="addRec" placeholder="Address Receive" required/>
                    <input type="number" className="form-control mb-1" ref="money" placeholder="Money" required/>
                    <input type="hidden" ref="time" value={Date.now()}/>
                    <div class="input-group">
                        <input type="text" class="form-control mb-1" ref="code" placeholder="Confirm code" required/>
                        <span class="input-group-btn mb-1">
                            <button class="btn btn-secondary" type="button" onClick={() => this.sendEmail()}>Get Confirm Code</button>
                        </span>
                    </div> 
                    <button className="btn btn-primary" onClick={() => this.transfer()}>Transfer</button>
                </div>
            </div>
        );
    }
    componentDidMount () {
        console.log('transfer api');
        this.getMoney();
    }
    getMoney() {
        var {dispatch} = this.props;
        axios.get('/kc_api/transfer')
            .then(function (rs) {
                if (rs) {
                    dispatch({type: 'get_money', data: rs.data});
                }
            })
            .catch(function (err) {
                console.log(err);
                dispatch({type: 'get_money', data: null});
            });
    }
}

module.exports = connect(function (state) {
    return {
        user: state.user
    };
})(Transfer);