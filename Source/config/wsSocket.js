const WebSocket = require('ws');

const ws = new WebSocket('wss://api.kcoin.club');

let wsArr = [];

ws.onopen = function () {
    console.log('ws connected');
    setInterval(function () {
	    ws.send("alive");
      console.log(Date.now() + " - Data sent!");
    }, 20000);
};

ws.onmessage = function (data) {
	let dt = JSON.parse(data.data);
    console.log('incoming data: ', dt.type);    
    if (dt.type == "block") {
    	console.log('incoming data: block');
    	if (global.Blocks.length > 0) {
    		if (wsArr.length > 0) {
    			for (let i = 0; i < wsArr.length; i++) {
    				global.Blocks.push(wsArr[i]);
    			}
    			wsArr = [];
    			console.log("Push unlucky data.");
    		}
	    	global.Blocks.push(dt.data);
	    	console.log("New Block length: " + global.Blocks.length);
	    	console.log("last Block: " + JSON.stringify(global.Blocks[global.Blocks.length - 1]).toString());
	    	//console.log(global.Blocks[global.Blocks.length - 1]);
    	}
    	else {
    		wsArr.push(dt.data);
    		console.log("Unlucky! " + wsArr.length);
    	}
    }
    else {
    	console.log('incoming data: not a block');
    }
};

