var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var User = require('../model/user.js');
var Address = require('../model/address.js');
var Key = require('../model/key.js');
var axios = require('axios');
const SHA256 = require('crypto-js/sha256');

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use('local.signup',  new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, done) {
    console.log('IUGIGUIGI');
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    req.checkBody('password', 'Invalid password').notEmpty().isLength({min: 4});
    var errors = req.validationErrors();
    if (errors) {
        var messages = [];
        messages.push(errors[0].msg);
        console.log(errors);
        return done(null, false, req.flash('error', messages));
    }
    req.sanitize('email').escape();
    //req.sanitize('username').escape();
    var email = req.body.email;
    console.log(email);
    var pw = req.body.password;
    if (pw.indexOf(' ') >= 0) {
        return done(null, false, {message: 'Invalid password'});
    }
    User.findOne({'email': email}, function (err, user) {
        if (err) {
            console.log(err);
            return done(err);
        }
        if (user) {
            console.log(user);
            return done(null, false, {message: 'Email is already in use.'});
        }
        console.log('create account');
        var newAcc = new User();
        newAcc.email = email;
        newAcc.password = newAcc.encyptPassword(password);
        axios.get("https://api.kcoin.club/generate-address")
               .then(function (rs) {
                    if (rs) {
                        console.log("generate-address");
                        let data = rs.data;
                        var add = new Address();
                        add.address = data.address;
                        var key = new Key();
                        key.privateKey = data.privateKey;
                        key.publicKey = data.publicKey;
                        newAcc.active = SHA256(Date.now() + generateString()).toString();
                        newAcc.save(function (err, result) {
                            if (err) {
                                console.log(err);
                                return done(err);
                            }
                            else {
                                console.log("save acc");
                                //console.log(result);
                                transporter.sendMail({
                                    from: 'noreplydawck2017@gmail.com',
                                    to: newAcc.email,
                                    subject: 'Hello ' + newAcc.email + '!',
                                    template: 'mail',
                                    context: {
                                        layout: false,
                                        link: "/active/" + newAcc.active
                                    }
                                }, function (errEmail, rs) {
                                    if (errEmail) {
                                        console.log(errEmail);
                                    }
                                });
                                add.userid = result._id;
                                key.userid = result._id;
                                key.save(function(errK, rsKey) {
                                    if (errK) {
                                        console.log(errK);
                                        return done(err);
                                    }
                                    else {
                                        console.log("save key");
                                        add.keyid = rsKey._id;
                                        add.save();
                                        return done(null, newAcc);
                                    }
                                });
                            }
                        });
                    }
               })
               .catch(function (err) {
                   console.log(err);
               });
    });
}));

passport.use('local.signin',  new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true
}, function (req, email, password, done) {
    req.checkBody('email', 'Invalid email').notEmpty().isEmail();
    req.checkBody('password', 'Invalid password').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        var messages = [];
        messages.push(errors[0].msg);
        return done(null, false, req.flash('error', messages));
    }
    User.findOne({'email': email}, function (err, user) {
        if (err) {
            console.log(err);
            return done(err);
        }
        if (!user) {
            console.log(user);
            console.log(email);
            return done(null, false, {message: 'Please check your username and password again.'});
        }
        if (!user.validPassword(password)) {
            return done(null, false, {message: 'Please check your username and password again.'});
        }
        if (user.active !== "ok") {
            return done(null, false, {message: 'Please active your account.'});
        }
        return done(null, user);
    });
}));

function generateString() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ ) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}