let axios = require('axios');

async function getTheBlock() {
    let url = "https://api.kcoin.club/blocks?&offset=";
    let result = [];
    let max = 1000;
    let each = [];
    const startOffset = 500;
    console.log("Bat dau lay Blocks");
    for (var i = startOffset; i < max; i+=100) {
        if (i == startOffset) {
            try {
                rs = await axios.get(url + i);
                if (rs) {
                    max  = rs.headers["x-total-count"];
                    //console.log(i);
                    result = result.concat(rs.data)
                }
            }
            catch (e) {
                console.log(e);
            }
        }
        else if (i > startOffset) {
            each.push(axios.get(url + i));
        }
    }
    for (let j = 0; j < each.length; j++) {
        try {
            //console.log(j);
            let rs = await each[j];
            result = result.concat(rs.data);
            //console.log(result.length);
        }
        catch (e) {
            console.log('each:' + e);
        }
    }
    console.log("max: " + max);
    return result;
}

async function theFunc() {
    global.Blocks = await getTheBlock();
    console.log("Block length: " + global.Blocks.length);
}

theFunc();