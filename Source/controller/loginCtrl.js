var passport = require('passport');
let User = require('../model/user');

exports.login_get = function (req, res, next) {
    var messages = req.flash('error');
    res.render('login', {layout: false, csrfToken:  req.csrfToken(), 
    	mess: messages[0], hasErr: messages.length > 0});
};

exports.login_post = passport.authenticate('local.signin', {
    failureRedirect: '/kcoin/login',
    failureFlash: true
});

exports.login_put = passport.authenticate('local.signup', {
    failureRedirect: '/kcoin/login',
    failureFlash: true
});

exports.logout = function (req, res, next) {
    req.logout();
    res.redirect('/kcoin');
}

exports.change_pass = function (req, res, next) {
	let result = {};
    result.status = 404;
    result.err = '';

    let cur = req.body.cur;
    let newP = req.body.newP;

    req.checkBody('newP', 'Invalid password').notEmpty().isLength({min: 4});
    let errors = req.validationErrors();
    if (errors) {
    	result.err = 'Invalid new password.';
    	res.json(result);
    	return;
    }
    User.findOne({email: req.user.email}, function (err, us) {
    	if (err || us == null) {
    		result.err = 'Please re-login.';
    		res.json(result);
    		return;
    	}
    	if (!us.validPassword(cur)) {
			result.err = 'Wrong current password.';
    		res.json(result);
    		return;
    	}
    	newP = us.encyptPassword(newP);
    	User.update({email: req.user.email}, {password: newP}, function (err2) {
    		if (err2) {
    			result.err = 'Cant change password.';
    			res.json(result);
    			return;
    		}
    		result.status = 200;
    		result.data = 'Change password success.';
    		res.json(result);
    	});
    });
}