var User = require('../model/user');
var Address = require('../model/address.js');
var Key = require('../model/key.js');
var Transaction = require('../model/transaction.js');
var Admin = require('../model/admin.js');
let Lib = require('./lib.js');

exports.admin_get = async function (req, res, next) {
	let chAM = await checkAdmin(req.user._id);
	if (!chAM) {
		res.redirect("/");
		return;
	}
	let totalUser = 0;
	let totalRealBL = 0;
	let totalAvaiBL = totalRealBL;
	User.find({}, async function (err, arr) {
		if (err || arr == null || arr.length <= 0) {
            console.log('cant get users');
    		res.render('adminDefault', {usNum: totalUser, realBL: totalRealBL, avaiBL: totalAvaiBL});
            return;
        }
        totalUser = arr.length;
        Address.find({}, async function (err2, arrAD) {
        	if (err2 || arrAD == null || arrAD.length <= 0) {
        		console.log('cant get address');
    			res.render('adminDefault', {usNum: totalUser, realBL: totalRealBL, avaiBL: totalAvaiBL});
            	return;
        	}
        	let arrBlock = await Lib.getBlock();
        	for (var i = 0; i < arrAD.length; i++) {
        		let mn = await Lib.calculateMoney(arrBlock, arrAD[i].address);
        		totalRealBL += mn;
                if (i == 0) {
                    totalAvaiBL += await Lib.calculateAvaiMoney(mn, arrAD[i].address);                    
                }
                else {                    
                    totalAvaiBL += await Lib.calculateAvaiMoney2(mn, arrAD[i].address);
                }
        	}
        	res.render('adminDefault', {usNum: totalUser, realBL: totalRealBL, avaiBL: totalAvaiBL});
        	return;
        });
	});
};

exports.admin_user_get = async function (req, res, next) {
	let chAM = await checkAdmin(req.user._id);
	if (!chAM) {
		res.redirect("/");
		return;
	}
	let usL = [];
	User.find({}, async function (err, arr) {
		if (err || arr == null || arr.length <= 0) {
            console.log('cant get users');
    		res.render('adminUser', {users: usL});
            return;
        }
        try {
        	for (var i = 0; i < arr.length; i++) {        		
	        	let obj = {};
	        	obj.email = arr[i].email;
	        	let arrAD = await Address.find({userid: arr[i]._id});
	        	if (arrAD.length > 0) {
	        		obj.address = arrAD[0].address;
	        		obj.rebl = 0;
	        		let arrBlock = await Lib.getBlock();
	        		obj.rebl = await Lib.calculateMoney(arrBlock, obj.address);
                    if (i == 0) {
                        obj.avbl = await Lib.calculateAvaiMoney(obj.rebl, obj.address);
                    }
                    else {
                        obj.avbl = await Lib.calculateAvaiMoney2(obj.rebl, obj.address);
                    }
	        	}
	        	usL.push(obj);
        	}
        	res.render('adminUser', {users: usL});
            return;
        }
        catch (e) {
        	console.log(e);
        }
	});
};

exports.admin_trans_get = async function (req, res, next) {
	let chAM = await checkAdmin(req.user._id);
	if (!chAM) {
		res.redirect("/");
		return;
	}
	let tranL = [];
	Transaction.find({}, function (err, arr) {
		if (err || arr == null || arr.length <= 0) {
            console.log('cant get transactions');
            res.render('adminTrans', {trans: tranL});
            return;
        }
        arr.sort(function (a, b) {
            return (b.time - a.time);
        });
        for (var i = 0; i < arr.length; i++) {
        	let obj = {};
        	obj.hash = arr[i].hash;
        	obj.status = arr[i].status;
        	obj.sder = arr[i].inpAD;
        	obj.rver = arr[i].outAD;
        	obj.amount = 0;
            for (let j = 0; j < arr[i].outputs.length; j++) {
                let add = arr[i].outputs[j].lockScript.slice(4);
                if (add === obj.rver) {
                    obj.amount += parseInt(arr[i].outputs[j].value);
                }
            }
            obj.time = new Date(arr[i].time);
        	tranL.push(obj);
    	}
    	res.render('adminTrans', {trans: tranL});
	});
};

async function checkAdmin(usid) {
	try {
		let arr = await Admin.find({userid: usid});
		if (arr == null || arr.length <= 0) {
			return false;
		}
		return true;
	}
	catch (e) {
		console.log(e);
		return false;
	}
}