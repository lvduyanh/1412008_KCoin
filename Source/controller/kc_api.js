var Counter = require('../model/counter');
var History = require('../model/history');
var User = require('../model/user');
var Address = require('../model/address.js');
var Key = require('../model/key.js');
var Transaction = require('../model/transaction.js');
var Wallet = require('../model/wallet');
let Lib = require('./lib.js');

let axios = require('axios');
const SHA256 = require('crypto-js/sha256');
const crypto = require('crypto');
//const ursa = require('ursa');

exports.info_get = function (req, res, next) {
    var result = {};
    result.status = 404;
    result.err = '';
    //console.log("Wow: " + req.user);
    User.findOne({email: req.user.email}, function (err, us) {
        if (err || us == null) {
            result.err = 'cant get user';
            console.log('cant get user');
            res.json(result);
        }
        else {
            Address.find({userid: us._id}, async function (err2, arr) {
                if (err2 || arr == null) {
                    result.err = 'cant get address';
                    console.log('cant get address');
                    res.json(result);
                }
                else {
                    result.user = {};
                    result.status = 200;
                    result.user.email = us.email;
                    let arrBlock = await Lib.getBlock();
                    console.log(arrBlock.length);
                    result.user.coin = await Lib.calculateMoney(arrBlock, arr[0].address);
                    result.user.avai_coin = await Lib.calculateAvaiMoney(result.user.coin, arr[0].address);
                    result.user.address = arr[0].address;
                    result.user.active = (us.active === 'ok') ? 'Activated' : 'Not activated';
                    //console.log(result.user);
                    res.json(result);
                }
            });
        }
    });
};

exports.received_get = function (req, res, next) {
    var result = {};
    result.status = 404;
    result.err = '';
    User.findOne({email: req.user.email}, function (err, us) {
        if (err || us == null) {
            result.err = 'cant get user';
            console.log('cant get user');
            res.json(result);
        }
        else {
            Address.find({userid: us._id}, async function (err2, arr) {
                if (err2 || arr == null || arr.length <= 0) {
                    result.err = 'cant get address';
                    console.log('cant get address');
                    res.json(result);
                }
                else {
                    try {
                        await Lib.fcUpdateTrans();
                        Transaction.find({outAD: arr[0].address}, function (err3, arrTran) {
                            if (err3 || arrTran == null || arrTran.length <= 0) {
                                result.trans = null;
                                result.address = arr[0].address;
                                result.status = 200;
                                console.log('cant get transactions');
                                res.json(result);
                            }
                            else {
                                result.address = arr[0].address;
                                result.status = 200;
                                result.trans = [];
                                arrTran.sort(function (a, b) {
                                    return (b.time - a.time);
                                });
                                for (let i = 0; i < arrTran.length; i++) {
                                    let tmp = {};
                                    tmp.status = arrTran[i].status;
                                    tmp.sender = arrTran[i].inpAD;
                                    tmp.hash = arrTran[i].hash;
                                    tmp.time = new Date(arrTran[i].time);
                                    let money = 0;
                                    for (let j = 0; j < arrTran[i].outputs.length; j++) {
                                        let add = arrTran[i].outputs[j].lockScript.slice(4);
                                        if (add === result.address) {
                                            money += parseInt(arrTran[i].outputs[j].value);
                                        }
                                    }
                                    tmp.money = money;
                                    result.trans.push(tmp);
                                }
                                res.json(result);
                            }
                        });
                    }
                    catch (e) {
                        console.log(e);
                    }
                }
            });
        }
    });
};

exports.sent_get = function (req, res, next) {
    var result = {};
    result.status = 404;
    result.err = '';
    User.findOne({email: req.user.email}, function (err, us) {
        if (err || us == null) {
            result.err = 'cant get user';
            console.log('cant get user');
            res.json(result);
        }
        else {
            Address.find({userid: us._id}, async function (err2, arr) {
                if (err2 || arr == null || arr.length <= 0) {
                    result.err = 'cant get address';
                    console.log('cant get address');
                    res.json(result);
                }
                else {
                    await Lib.fcUpdateTrans();
                    Transaction.find({inpAD: arr[0].address}, function (err3, arrTran) {
                        if (err3 || arrTran == null || arrTran.length <= 0) {
                            result.trans = null;
                            result.status = 200;
                            console.log('cant get transactions');
                            res.json(result);
                        }
                        else {
                            result.status = 200;
                            result.trans = [];
                            arrTran.sort(function (a, b) {
                                return (b.time - a.time);
                            });
                            for (let i = 0; i < arrTran.length; i++) {
                                let tmp = {};
                                tmp.status = arrTran[i].status;
                                tmp.receiver = arrTran[i].outAD;
                                tmp.hash = arrTran[i].hash;
                                tmp.time = new Date(arrTran[i].time);
                                let money = 0;
                                for (let j = 0; j < arrTran[i].outputs.length; j++) {
                                    let add = arrTran[i].outputs[j].lockScript.slice(4);
                                    if (add === tmp.receiver) {
                                        money += parseInt(arrTran[i].outputs[j].value);
                                    }
                                }
                                tmp.money = money;
                                result.trans.push(tmp);
                            }
                            res.json(result);
                        }
                    });
                }
            });
        }
    });
};

exports.code_post = function (req, res, next) {
    var result = {};
    result.status = 404;
    result.err = '';
    let ad = req.body.address;
    let time = req.body.time;
    let code = SHA256(ad + time).toString();
    transporter.sendMail({
        from: 'noreplydawck2017@gmail.com',
        to: req.user.email,
        subject: 'Confirm code',
        template: 'mail2',
        context: {
            layout: false,
            code: code
        }
    }, function (errEmail, rs) {
        if (errEmail) {
            console.log(errEmail);
            //result.err = 'Cant send email.';
            //res.json(result);
        }
    });
    result.status = 200;
    result.data = "Please check confirm code in your email box!";
    res.json(result);
};


exports.transfer_post =  function (req, res, next) {
    console.log("transfer_post");
    var result = {};
    result.status = 404;
    result.err = '';
    console.log(req.body);
    let ad = req.body.addRec;
    let money = req.body.money;
    let time = req.body.time;
    let code = req.body.code;
    let code2 = SHA256(ad + time).toString();
    if (false || code !== code2) {
        result.err = 'Invalid confirm code.';
        res.json(result);
    }
    else {
        User.findOne({email: req.user.email}, function (err, us) {
            if (err || us == null) {
                result.err = 'Cant get user';
                console.log('cant get user');
                res.json(result);
            }
            else {
                Address.find({userid: us._id}, function (err2, arr) {
                    if (err2 || arr == null) {
                        result.err = 'Cant get address';
                        console.log('cant get address');
                        res.json(result);
                    }
                    else {
                        var currAd = arr[0].address;
                        Key.find({userid: us._id}, async function (err3, arrK) {
                            if (err3 || arrK == null) {
                                result.err = 'Cant get key';
                                console.log('cant get key');
                                res.json(result);
                            }
                            else {
                                const priK = arrK[0].privateKey;
                                const pubK = arrK[0].publicKey;
                                let arrBlock = await Lib.getBlock();
                                console.log(arrBlock.length);
                                let avai_coin = await Lib.calculateMoney(arrBlock, currAd);
                                avai_coin = await Lib.calculateAvaiMoney(avai_coin, currAd);
                                if (avai_coin < money) {
                                    result.err = 'You dont have enought money.';
                                    res.json(result);
                                    return;
                                }
                                let obj = Lib.findOutIdRef(arrBlock, currAd);
                                if (obj == null || obj.length <= 0) {
                                    result.err = 'Cant find referencedOutputHash.';
                                    res.json(result);
                                    return;
                                }
                                console.log(obj);
                                let transaction = {};
                                transaction.version = 1;
                                transaction.hash = "";
                                transaction.inputs = [];
                                for (let ir = 0; ir < obj.length; ir++) {
                                    transaction.inputs.push({
                                        unlockScript: "",
                                        referencedOutputHash: obj[ir].hash,
                                        referencedOutputIndex: obj[ir].id
                                    });
                                }
                                transaction.outputs = [{
                                    value: avai_coin - money,
                                    lockScript: "ADD " + currAd
                                },
                                {
                                    value: money,
                                    lockScript: "ADD " + ad
                                }];
                                let mess = Lib.toBinary(transaction, true);
                                //console.log(mess);
                                // let pk = ursa.createPrivateKey(Buffer.from(privateKey, 'hex'));
                                // let signer = ursa.createSigner('sha256');
                                // signer.update(mess);
                                // let signature = signer.sign(pk, 'hex');
                                // console.log(signature);
                                const sign = crypto.createSign("SHA256");
                                sign.update(mess);
                                let signature = sign.sign(Buffer.from(priK, 'hex'), 'hex');
                                console.log(signature);
                                console.log("fff");
                                let unScript = "PUB " + pubK + " SIG " + signature;
                                for (var iIP = 0; iIP < transaction.inputs.length; iIP++) {
                                    transaction.inputs[iIP].unlockScript = unScript;
                                }
                                console.log(JSON.stringify(transaction).toString());
                                axios.post('https://api.kcoin.club/transactions', {
                                    inputs: transaction.inputs,
                                    outputs: transaction.outputs,
                                    version: 1
                                })
                                .then(function(rs) {
                                    let data = rs.data;
                                    console.log(data);
                                    if (data.hash) {
                                        let tranMG = new Transaction();
                                        tranMG.status = "Wating";
                                        tranMG.version = 1;
                                        tranMG.time = Date.now();
                                        tranMG.inpAD = currAd;
                                        tranMG.outAD = ad;
                                        tranMG.hash = data.hash;
                                        tranMG.inputs = transaction.inputs;
                                        tranMG.outputs = transaction.outputs;
                                        tranMG.save();
                                    }                                    
                                    result.status = 200;
                                    result.data = "Success.";
                                    res.json(result);
                                })
                                .catch(function(err) {
                                    console.log(err);
                                    result.err = 'Error whent send transaction.';
                                    res.json(result);
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};

exports.transfer_get = function (req, res, next) {
    var result = {};
    result.status = 404;
    result.err ='';
    Address.find({userid: req.user._id}, async function (err, arr) {
        if (err || arr == null) {
            result.err = 'cant get address';
            console.log('cant get address');
            res.json(result);
        }
        else {
            result.user = {};
            result.status = 200;
            result.user.email = req.user.email;
            let arrBlock = await Lib.getBlock();
            console.log(arrBlock.length);
            result.user.coin = await Lib.calculateMoney(arrBlock, arr[0].address);
            result.user.avai_coin = await Lib.calculateAvaiMoney(result.user.coin, arr[0].address);
            console.log(result.user);
            res.json(result);
        }
    });
};