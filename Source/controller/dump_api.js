var Counter = require('../model/counter');
var History = require('../model/history');
var User = require('../model/user');
var Wallet = require('../model/wallet');

exports.transfer_post =  function (req, res, next) {
    var rs = {};
    rs.status = 404;
    rs.err = '';
    if (req.isAuthenticated()) {
        console.log(req.body);
        var uid = req.body.userid;
        var coin = parseInt(req.body.money);
        User.findOne({email: req.user.email}, function (err, us) {
            console.log(us);
            if (!err) {
                console.log(uid);
                if (uid == us._id) {
                    rs.err = 'Cant tranfer to yourself.';
                    res.json(rs);
                    return;
                }
                User.findById(uid, function (err, us2) {
                    if (err || us2 == null) {
                        console.log('fff');
                        rs.err = 'Cant find user';
                        res.json(rs);
                    }
                    else {
                        console.log(us2);
                        Wallet.findOne({user: us.email}, function (err, wu1) {
                            if (err) {
                                console.log(err);
                                rs.err = 'Cant load your wallet.';
                                res.json(rs);
                            }
                            else {
                                Wallet.findOne({user: us2.email}, function (err, wu2) {
                                    if (err) {
                                        console.log(err);
                                        rs.err = 'Cant load opponent wallet.';
                                        res.json(rs);
                                    }
                                    else {
                                        Counter.find({}, function (err, rsct) {
                                            var hn;
                                            if (err || rsct == null) {
                                                console.log('couter');
                                                rs.err = 'Cant save history.';
                                                res.json(rs);
                                                return;
                                            }
                                            else if (rsct.length <= 0) {
                                                var cter = new Counter();
                                                cter.hisNum = 1;
                                                cter.save();
                                                hn = 1;
                                            }
                                            else {
                                                hn = rsct[0].hisNum + 1;
                                                console.log(rsct);
                                                Counter.update({"_id": rsct[0]._id}, {"hisNum": hn}, function (err) {
                                                    console.log(hn);
                                                });
                                            }
                                            console.log(hn);
                                            if (wu1.coin >= coin) {
                                                wu1.coin -= coin;
                                                wu2.coin += coin;
                                                wu1.save();
                                                wu2.save();
                                                var htr = new History();
                                                htr.user1 = us.email;
                                                htr.user2 = us2.email;
                                                htr.coin = coin;
                                                htr.num = hn;
                                                htr.save();
                                                rs.status = 200;
                                                rs.data = 'Transfer ' + coin + ' VND to ' + us2.email + ' success!';
                                                res.json(rs);
                                            }
                                            else {
                                                rs.err = 'Your money is not enought.';
                                                res.json(rs);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
            else {
                console.log(err);
                rs.err = 'Login again, please!';
                res.json(rs);
            }
        });
    }
    else {
        rs.err = 'You are not logged in';
        res.json(rs);
    }
};

exports.transfer_get = function (req, res, next) {
    var result = {};
    result.status = 404;
    result.err ='';
    Wallet.findOne({user: req.user.email}, function (err, usw) {
        console.log(usw);
        if (err || usw == null) {
            console.log('cant get wallet');
            res.json(result);
        }
        else {
            result.status = 200;
            result.user = {};
            result.user.email = req.user.email;
            result.user.coin = usw.coin;
            res.json(result);
        }
    }); 
};

exports.all_get = function (req, res, next) {
    console.log('hello');
    var result = {};
    result.status = 404;
    result.err ='';
    History.find({}, function (err, arr) {
        if (err) {
            console.los(err);
            result.err = 'Cant load data.';
            res.json(result);
        }
        else {
            result.status = 200;
            arr.sort(function (a, b) {
                return (b.num - a.num);
            });
            result.data = arr;
            res.json(result);
        }
    });
};

exports.recent_get = function (req, res, next) {
    var result = {};
    result.status = 404;
    result.err ='';
    Wallet.findOne({user: req.user.email}, function (err, usw) {
        console.log(usw);
        if (err || usw == null) {
            console.log('cant get wallet');
            res.json(result);
        }
        else {
            History.find({
                $or: [
                    {user1: req.user.email},
                    {user2: req.user.email}
                ]}, function (err, arr) {
                if (err) {
                    console.log(err);
                    result.err = 'Cant load data.';
                    res.json(result);
                }
                else {
                    result.status = 200;
                    arr.sort(function (a, b) {
                        return (b.num - a.num);
                    });
                    result.user = {};
                    result.user.email = req.user.email;
                    result.user.id = req.user._id;
                    result.user.coin = usw.coin;
                    result.data = arr;
                    res.json(result);
                }
            });
        }
    });
};