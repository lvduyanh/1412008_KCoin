let Transaction = require('../model/transaction.js');
let axios = require('axios');

exports.getBlock = async function () {
    return global.Blocks;
}

exports.calculateMoney = async function (arr, adr) {
    console.log("Bat dau tinh tien!");
    let money = 0;
    let arrOutHash = [];
    let arrOutId = [];
    // mang referentOutputHash
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].transactions.length; j++) {
            for (var k = 0; k < arr[i].transactions[j].inputs.length; k++) {
                if (arr[i].transactions[j].inputs[k].referencedOutputIndex !== -1) {
                    arrOutId.push(arr[i].transactions[j].inputs[k].referencedOutputIndex);
                    arrOutHash.push(arr[i].transactions[j].inputs[k].referencedOutputHash);
                }
            }
        }
    }
    // tinh tien
    for (var i = 0; i < arr.length; i++) {
        for (var j = 0; j < arr[i].transactions.length; j++) {
            const currTransHash = arr[i].transactions[j].hash;
            let id = arrOutHash.indexOf(currTransHash);
            for (var k = 0; k < arr[i].transactions[j].outputs.length; k++) {
                if (id === -1) {
                    let add = arr[i].transactions[j].outputs[k].lockScript.slice(4);
                    if (add === adr) {
                        money += parseInt(arr[i].transactions[j].outputs[k].value);
                        console.log("id: " + id + " k: " + k + " mn: " + parseInt(arr[i].transactions[j].outputs[k].value));
                    }
                }
                else {
                    let add = arr[i].transactions[j].outputs[k].lockScript.slice(4);
                    if (add === adr) {
                        // kiem tra output da duoc xai chua
                        let arrOutIndex = [];
                        for (let ido = 0; ido < arrOutHash.length; ido++) {
                            if (arrOutHash[ido] === currTransHash) {
                                arrOutIndex.push(arrOutId[ido]);
                            }
                        }
                        let check = arrOutIndex.indexOf(k);
                        if (check === -1) {
                            money += parseInt(arr[i].transactions[j].outputs[k].value);
                            console.log("out id:" + arrOutId[id]);
                            console.log("id: " + id + " k: " + k + " mn: " + parseInt(arr[i].transactions[j].outputs[k].value));
                        }
                        else
                            console.log(check + " k: " + arrOutIndex[check]);

                    }
                }
            }
        }
    }
    return money;
}

exports.findOutIdRef = function (arr, adr) {
  let arrRs = [];
  let arrOutHash = [];
  let arrOutId = [];
  // mang referentOutputHash
  for (var i = 0; i < arr.length; i++) {
      for (var j = 0; j < arr[i].transactions.length; j++) {
          for (var k = 0; k < arr[i].transactions[j].inputs.length; k++) {
              if (arr[i].transactions[j].inputs[k].referencedOutputIndex !== -1) {
                  arrOutId.push(arr[i].transactions[j].inputs[k].referencedOutputIndex);
                  arrOutHash.push(arr[i].transactions[j].inputs[k].referencedOutputHash);
              }
          }
      }
  }
  for (var i = 0; i < arr.length; i++) {
    for (var j = 0; j < arr[i].transactions.length; j++) {
        const currTransHash = arr[i].transactions[j].hash;
        let id = arrOutHash.indexOf(currTransHash);
        for (var k = 0; k < arr[i].transactions[j].outputs.length; k++) {
            if (id === -1) {
                let add = arr[i].transactions[j].outputs[k].lockScript.slice(4);
                if (add === adr) {
                  let rs = {};
                  rs.hash = currTransHash;
                  rs.id = k;
                  arrRs.push(rs);                                  
                }
            }
            else {
                let add = arr[i].transactions[j].outputs[k].lockScript.slice(4);
                if (add === adr) {
                    // kiem tra output da duoc xai chua
                    let arrOutIndex = [];
                    for (let ido = 0; ido < arrOutHash.length; ido++) {
                        if (arrOutHash[ido] === currTransHash) {
                            arrOutIndex.push(arrOutId[ido]);
                        }
                    }
                    let check = arrOutIndex.indexOf(k);
                    if (check === -1) {
                      let rs = {};
                      rs.hash = currTransHash;
                      rs.id = k;
                      arrRs.push(rs);
                    }
                    else {
                      // output da duoc xai
                    }

                }
            }
        }
    }
  }
  return arrRs;
}

exports.toBinary = function (transaction, withoutUnlockScript) {
let version = Buffer.alloc(4);
version.writeUInt32BE(transaction.version);
let inputCount = Buffer.alloc(4);
inputCount.writeUInt32BE(transaction.inputs.length);
let inputs = Buffer.concat(transaction.inputs.map(input => {
  // Output transaction hash
  let outputHash = Buffer.from(input.referencedOutputHash, 'hex');
  // Output transaction index
  let outputIndex = Buffer.alloc(4);
  // Signed may be -1
  outputIndex.writeInt32BE(input.referencedOutputIndex);
  let unlockScriptLength = Buffer.alloc(4);
  // For signing
  if (!withoutUnlockScript) {
    // Script length
    unlockScriptLength.writeUInt32BE(input.unlockScript.length);
    // Script
    let unlockScript = Buffer.from(input.unlockScript, 'binary');
    return Buffer.concat([ outputHash, outputIndex, unlockScriptLength, unlockScript ]);
  }
  // 0 input
  unlockScriptLength.writeUInt32BE(0);
  return Buffer.concat([ outputHash, outputIndex, unlockScriptLength]);
}));
let outputCount = Buffer.alloc(4);
outputCount.writeUInt32BE(transaction.outputs.length);
let outputs = Buffer.concat(transaction.outputs.map(output => {
  // Output value
  let value = Buffer.alloc(4);
  value.writeUInt32BE(output.value);
  // Script length
  let lockScriptLength = Buffer.alloc(4);
  lockScriptLength.writeUInt32BE(output.lockScript.length);
  // Script
  let lockScript = Buffer.from(output.lockScript);
  return Buffer.concat([value, lockScriptLength, lockScript ]);
}));
return Buffer.concat([ version, inputCount, inputs, outputCount, outputs ]);
};

let UpdateTrans = async function () {
  let arrTranCheck = await Transaction.find({});
  if (arrTranCheck == null || arrTranCheck.length <= 0) {
    console.log("no arrTranCheck.");
    return;
  }
  let check = true;
  for (let i = 0; i < arrTranCheck.length; i++) {
    if (arrTranCheck[i].status !== "Finished") {
      check = false;
      break;
    }
  }
  if (check) {
    console.log("No need to UpdateTrans!");
    return;
  }
  let unTran = await axios.get("https://api.kcoin.club/unconfirmed-transactions");
  unTran = unTran.data;
  try {
    let arrTran = await Transaction.find({});
    if (arrTran == null || arrTran.length <= 0) {
      console.log("UpdateTrans: get trans err.");
      return;
    }
    else {
      console.log("UpdateTrans");
      if (unTran == null || unTran.length <= 0) {
        for (let i = 0; i < arrTran.length; i++) {
          if (arrTran[i].status !== "Finished") {
            await Transaction.update({"_id": arrTran[i]._id}, {"status": 'Finished'});
          }
        }
        console.log("UpdateTrans finish");
        return;
      }
      else {
        for (let i = 0; i < arrTran.length; i++) {
          if (arrTran[i].status !== "Finished") {
            let isInUT = false;
            for (let j = 0; j < unTran.length; j++) {
              if (unTran[j].hash === arrTran[i].hash) {
                isInUT = true;
                break;
              }
            }
            if (!isInUT) {
              console.log("Update a trans.");
              await Transaction.update({"_id": arrTran[i]._id}, {"status": 'Finished'});
            }
          }
        }
      }
    }
  }
  catch (err) {
    console.log(err);
    console.log("UpdateTrans: get trans err.");
    return;
  }
}

exports.fcUpdateTrans = UpdateTrans;

exports.calculateAvaiMoney = async function (realMN, adr) {
  let money = realMN;
  console.log("avai: " + money);
  try {
    await UpdateTrans();
    let arrTran = await Transaction.find({inpAD: adr});
    if (arrTran == null || arrTran.length <= 0) {
      console.log("calculateAvaiMoney: get trans err.");
      return money;
    }
    else {
      console.log("tinh avai money");
      for (let i = 0; i < arrTran.length; i++) {
        if (arrTran[i].status !== "Finished") {
          let adRece = arrTran[i].outAD;
          for (let j = 0; j < arrTran[i].outputs.length; j++) {
              let add = arrTran[i].outputs[j].lockScript.slice(4);
              if (add === adRece) {
                  //console.log("avai2: " + money);
                  money -= parseInt(arrTran[i].outputs[j].value);
                  //console.log("avai3: " + money);
              }
          }
        }
      }
      console.log("avai4: " + money);
      return money;
    }

  }
  catch (err) {
    console.log(err);
    console.log("calculateAvaiMoney: get trans err.");
    return money;
  }
}

exports.calculateAvaiMoney2 = async function (realMN, adr) {
  let money = realMN;
  console.log("avai: " + money);
  try {
    let arrTran = await Transaction.find({inpAD: adr});
    if (arrTran == null || arrTran.length <= 0) {
      console.log("calculateAvaiMoney: get trans err.");
      return money;
    }
    else {
      console.log("tinh avai money");
      for (let i = 0; i < arrTran.length; i++) {
        if (arrTran[i].status !== "Finished") {
          let adRece = arrTran[i].outAD;
          for (let j = 0; j < arrTran[i].outputs.length; j++) {
              let add = arrTran[i].outputs[j].lockScript.slice(4);
              if (add === adRece) {
                  //console.log("avai2: " + money);
                  money -= parseInt(arrTran[i].outputs[j].value);
                  //console.log("avai3: " + money);
              }
          }
        }
      }
      console.log("avai4: " + money);
      return money;
    }

  }
  catch (err) {
    console.log(err);
    console.log("calculateAvaiMoney: get trans err.");
    return money;
  }
}