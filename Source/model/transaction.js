var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = Schema(
    {
        status: {type: String, required: true},
        inpAD: {type: String, required: true},
        outAD: {type: String, required: true},
        hash: {type: String, required: true},
        version: {type: Number, default: 1},
        inputs: {type: Array, required: true},
        outputs: {type: Array, required: true},
        time: {type: Number, required: true}
    }
);

module.exports = mongoose.model('Transaction', schema);