var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var counterSchema = Schema(
    {
        hisNum: {type: Number, required: true}
    }
);

module.exports = mongoose.model('Counter', counterSchema);