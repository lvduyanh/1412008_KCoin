var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var walletSchema = Schema(
    {
        user: {type: String, required: true},
        coin: {type: Number, default: 1000}
    }
);

module.exports = mongoose.model('Wallet', walletSchema);