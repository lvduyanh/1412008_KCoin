var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var userSchema = Schema(
    {
        email: {type: String, required: true},
        password: {type: String, required: true},
        active: {type: String, required: true},
        name: {type: String, default: ''}
    }
);

userSchema.methods.encyptPassword = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(5), null);
};

userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('User', userSchema);