var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var historySchema = Schema(
    {
        user1: {type: String, required: true},
        user2: {type: String, required: true},
        coin: {type: Number, default: 0},
        num: {type: Number, default: 0}
    }
);

module.exports = mongoose.model('History', historySchema);