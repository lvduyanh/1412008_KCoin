var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = Schema(
    {
        userid: {type: String, required: true}
    }
);

module.exports = mongoose.model('Admin', schema);