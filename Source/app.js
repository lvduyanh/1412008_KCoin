var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var passport = require('passport');
var flash = require('connect-flash');
var validator = require('express-validator');

var index = require('./routes/index');
var users = require('./routes/users');
var kcoin = require('./routes/kcoin');
var kc_api = require('./routes/api_kcoin');
var admin = require('./routes/admin');

var app = express();

// connect mongodb
var mongoDB = 'mongodb://127.0.0.1/KCoin_1412008';
mongoose.connect(mongoDB);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// mail service
var nodemailer = require('nodemailer');
var nehbs = require('nodemailer-express-handlebars');
global.transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    auth: {
        user: 'noreplydawck2017@gmail.com',
        pass: 'dawck2017'
    }
});
transporter.use('compile', nehbs({
    viewPath: __dirname + '/views',
    extName: '.hbs'
}));

// authentication
require('./config/passport');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(validator());
app.use(session({
    secret: 'gogopikachu',
    resave: false,
    saveUninitialized: false,
    cookie: {maxAge: 3 * 60 * 60 * 1000}
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

// get blocks
global.Blocks = [];
require('./config/blocks');

// web socket
require('./config/wsSocket');

app.use(function (req, res, next) {
    res.locals.login = req.isAuthenticated();
    res.locals.session = req.session;
    next();
});

app.use('/', index);
app.use('/users', users);
app.use('/kcoin', kcoin);
app.use('/kc_api', kc_api);
app.use('/admin', admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
